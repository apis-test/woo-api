<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Pixelpeter\Woocommerce\Facades\Woocommerce;

class TestApiController extends Controller
{
    public function index()
    {
        return Woocommerce::get('orders');
    }
}
